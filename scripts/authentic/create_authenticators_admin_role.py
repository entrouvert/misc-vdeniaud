import logging

from authentic2.a2_rbac.models import Role
from authentic2.apps.authenticators.models import BaseAuthenticator
from django.contrib.contenttypes.models import ContentType

logger = logging.getLogger(__name__)


try:
    admin_role = Role.objects.get(slug='_a2-manager')
except Role.DoesNotExist:
    logger.warning('No admin role.')
else:
    content_type = ContentType.objects.get_for_model(BaseAuthenticator)
    role = Role.objects.get_admin_role(
        instance=content_type,
        slug='_a2-manager-of-authenticators',
        name='Administrateur des moyens d’authentification',
        update_name=True,
        update_slug=True,
        create=True,
    )
    role.add_child(admin_role)
