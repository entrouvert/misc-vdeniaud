from chrono.agendas.models import Event
from django.db import connection
from django.db.models import Count

duplicated_events = (
    Event.objects.values('primary_event', 'start_datetime')
    .annotate(count=Count('id'))
    .values('pk')
    .order_by()
    .filter(count__gt=1)
)
if duplicated_events.count():
    print(connection.schema_name, duplicated_events)
