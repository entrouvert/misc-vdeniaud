from chrono.agendas.models import TimePeriod
from django.db import connection

periods = TimePeriod.objects.filter(agenda__isnull=False, weekday_indexes__isnull=False)
if periods.exists():
    print(connection.schema_name, [tp.agenda.get_absolute_url() for tp in periods])
