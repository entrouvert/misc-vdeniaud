from combo.apps.dataviz.models import ChartNgCell
from django.conf import settings

base_url = settings.SITE_BASE_URL
qs = ChartNgCell.objects.filter(
    validity_info__invalid_since__isnull=True, statistic__isnull=False, chart_type='table'
)
for cell in qs:
    try:
        chart = cell.get_chart()
    except:
        continue
    transpose = bool(chart.axis_count == 2)
    if transpose:
        print(
            'Switch to "table inverted" on cell %s from page %s'
            % (cell.pk, settings.SITE_BASE_URL + cell.page.get_online_url())
        )
        cell.chart_type = 'table-inverted'
        cell.save()
