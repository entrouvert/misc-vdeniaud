import collections
import json
import logging
import sys

from django.utils import timezone
from quixote import get_publisher
from wcs import sql
from wcs.sql_criterias import Equal

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('wcs_fusion.log')
fh.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

logger.addHandler(fh)
logger.addHandler(ch)


logger.info('=== Starting form reattachment at %s ===', timezone.now().strftime('%Y-%m-%dT%H:%M:%S'))

with open('authentic_fusion_result.json') as f:
    disabled_users_map = json.loads(f.read())


def get_user_by_uuid(uuid):
    users = get_publisher().user_class.get_users_with_name_identifier(uuid)

    if len(users) == 0:
        raise ValueError('no user found for uuid %s' % uuid)

    if len(users) > 1:
        raise ValueError('multiple users found for uuid %s' % uuid)

    return users[0]


disabled_users_by_user = collections.defaultdict(list)
for user_uuid, disabled_user_uuids in disabled_users_map.items():
    user = get_user_by_uuid(user_uuid)

    for disabled_user_uuid in disabled_user_uuids:
        disabled_users_by_user[user].append(get_user_by_uuid(disabled_user_uuid))

forms_by_user = collections.defaultdict(list)
for user, disabled_users in disabled_users_by_user.items():
    for disabled_user in disabled_users:
        forms_by_user[user].extend(sql.AnyFormData.select([Equal('user_id', str(disabled_user.id))]))


def attach_forms_to_user(forms_by_user):
    for user, forms in forms_by_user.items():
        for form in forms:
            logger.info(
                'Attaching form %s from user %s to user %s (%s)', form.id_display, form.user_id, user.id, user
            )
            form.user_id = user.id
            form.store()


try:
    with sql.atomic():
        attach_forms_to_user(forms_by_user)

        if len(sys.argv) < 2 or sys.argv[1] != '--proceed=true':
            raise ValueError

    logger.info('=== Success ===')
except ValueError:
    logger.info('=== Did nothing ===')
