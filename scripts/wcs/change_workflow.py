from wcs.formdef import FormDef
from wcs.qommon.storage import NotEqual
from wcs.workflows import Workflow

FORM_SLUG = 'Prime-eco-logis-91'
TARGET_WF_ID = 21

formdef = FormDef.get_by_urlname(FORM_SLUG)
print('Form:', formdef.name)
current_workflow = formdef.workflow
print('Current workflow:', current_workflow.name)
target_workflow = Workflow.get(TARGET_WF_ID)
print('Target workflow:', target_workflow.name, end='\n\n')

# remap statuses
print('Available statuses:')
for i, status in enumerate(target_workflow.possible_status):
    print('[%s] %s' % (i, status.name))
print('')

status_mapping = {}
status_mapping_display = {}
for status in current_workflow.possible_status:
    new_status = [x for x in target_workflow.possible_status if x.id == status.id]
    if not new_status:
        status_index = input('Map status %s to: ' % status.name)
        new_status = target_workflow.possible_status[int(status_index)]
    else:
        assert len(new_status) == 1
        new_status = new_status[0]
    status_mapping['wf-%s' % status.id] = 'wf-%s' % new_status.id
    status_mapping_display[status.name] = new_status.name

for status, target_status in status_mapping_display.items():
    print('Will remap status %s to %s' % (status, target_status))

if not input('Proceed? [y/N] ') == 'y':
    print('Aborted.')
    exit()

###
### workflow_status_remapping_submit code from wcs
###
for item in formdef.data_class().select([NotEqual('status', 'draft')]):
    item.status = status_mapping.get(item.status)
    if item.evolution:
        for evo in item.evolution:
            evo.status = status_mapping.get(evo.status)
    item.store()

formdef.workflow = Workflow.get(TARGET_WF_ID)
formdef.store(comment='Changement de workflow (manuel)')
# instruct formdef to update its security rules
formdef.data_class().rebuild_security()
formdef.update_storage()
