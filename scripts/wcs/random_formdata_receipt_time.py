import datetime
import random

from wcs.formdef import FormDef

FORMDEF = 'contactez-nous'
START_DATE = datetime.date(2019, 1, 1)

formdef = FormDef.get_by_urlname(FORMDEF)
data_class = formdef.data_class()
formdatas = data_class.select()
random.shuffle(formdatas)

hours = []
for hour in (12, 18):
    for _ in range(1000):
        random_hour = int(random.gauss(hour, 4))
        if 0 < random_hour < 24:
            hours.append(random_hour)

max_days = (datetime.datetime.now().date() - START_DATE).days

for formdata in formdatas:
    days = random.randrange(max_days)
    date = START_DATE + datetime.timedelta(days=days)

    # avoid totally unifrom date distribution
    if date.weekday() in (1, 3, 5) and random.randint(1, 2) % 2:
        date = START_DATE + datetime.timedelta(days=days)

    if not date.month % 3 and random.randint(1, 2) % 2:
        date = START_DATE + datetime.timedelta(days=days)

    if not date.year % 2 and random.randint(1, 2) % 2:
        date = START_DATE + datetime.timedelta(days=days)

    minute = random.randrange(60)
    hour = random.choice(hours)
    time = datetime.time(hour=hour, minute=minute)

    formdata.receipt_time = formdata.last_update_time = datetime.datetime.combine(date, time).timetuple()
    formdata.store()
